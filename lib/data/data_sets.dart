class DataSets {
  List<String> sectionNamesTwoFifty = [
    "2017 - 2018",
    "2016 - 2017",
    "2014 - 2015",
    "2013 - 2014",
    "2012 - 2013",
    "2011 - 2012",
    "2010 - 2011",
    "2009 - 2010",
    "2008 - 2009",
    "2007 - 2008",
    "2006 - 2007",
    "2005 - 2006",
    "2004 - 2005",
  ];

  getSectionNamesTwoFifty() {
    return this.sectionNamesTwoFifty;
  }

  List<String> sectionNamesFourFifty = [
    "2017 - Present",
    "2016 - Present",
    "2014 - 2015",
    "2013 - 2014",
    "2012 - 2013",
    "2011 - 2012",
    "Leo Vince Limited Edition 2011-2012",
    "2010 - 2011",
    "2009 - 2010",
    "2008 - 2009",
    "2007 - 2008",
    "2006 - 2007",
    "2005 - 2006",
  ];

  getSectionNamesFourFifty() {
    return this.sectionNamesFourFifty;
  }

}