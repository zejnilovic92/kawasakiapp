import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataSixSeven {
  static BikeObject bike1 = BikeObject(
    "KX250F 2006 - 2007",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/six_seven/img1.png",
      "assets/images/kawasaki_two_fifty/six_seven/img2.png",
      "assets/images/kawasaki_two_fifty/six_seven/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
"The good old 2006 Kawasaki KX250F 249cc 4-stroker just got better and can deliver a new level of performance, thanks to its revised carburetor, engine and gearbox.",
          "The updated piston, intake valves and polished intake ports make sure the fuel and air mix flow faster and steadily, never letting you down.",
        "With a new radiator in place, this motocross machine will keep within the optimal working parameters even in long, hard races. The racing-like aluminium skid plate is taller in front of the engine to prevent mud from accumulating and cause trouble.",
        "A 16-way adjustable forks provide excellent damping on any surface, no matter how hard you hit it, while the new, adjustable Uni-Trak rear suspension complements it in a spectacularly efficient manner. Petal disc brakes are used to make sure there is enough stopping force for the most demanding riding styles, while their design keeps brakes clean and in good condition during longer races."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.5:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: Carburettor: Keihin FCR37",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 47mm upside-down cartridge-type telescopic fork with 16-way compression and 16-way rebound damping",
          "REAR SUSPENSION: New Uni-Trak with adjustable preload, dual-speed (low: 13-way, high: 2-turns or more) compression damping and 17-way rebound damping",
          "FRONT BRAKE: Single semi-floating 250 mm disc, Dual-piston",
          "REAR BRAKE: Single 240 mm disc, Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85 in OR 2159 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: 57.8 in OR 1468 mm",
          "GROUND CLEARANCE: -",
          "WEIGHT: 204 lbs OR 93 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}