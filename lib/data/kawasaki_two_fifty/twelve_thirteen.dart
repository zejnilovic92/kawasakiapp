import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataTwelveThirteen {
  static BikeObject bike1 = BikeObject(
    "KX250F 2012 - 2013",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/twelve_thirteen/img1.png",
      "assets/images/kawasaki_two_fifty/twelve_thirteen/img2.png",
      "assets/images/kawasaki_two_fifty/twelve_thirteen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2012 Kawasaki KX250F boasts revised pistons and a new, lighter throttle body which adds a dramatic thrill to revving up the engine: with the body opening quick after 3/8 throttle, the rider should expect an explosion of power and torque, perfect for fast acceleration or for that sought-after mid-turn brawn.",
          "The KX FI Calibration Kit provides custom mappings to transform the 2012 Kawasaki KX250F into a racing machine that's especially adapted to each rider's preferences and style.",
"The separate function Showa fork has been finely tuned: with damping for the left leg and spring for the right one, the new front suspension delivers a smooth, firm action that's hard to reproduce in conventional forks, perfectly matching the fully-adjustable rear shock."
          ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.8:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin) with dual injection",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: Type:48 mm upside-down telescopic Separate Function front Fork (SFF) Type 2 Compression damping: 22-way Rebound damping: 20-way Spring preload: 40-way",
          "REAR SUSPENSION: Type: New Uni-Trak Compression damping: 19-way (low-speed), 4-turns (high-speed) Rebound damping: 22-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single semi-floating 250 mm disc Caliper, Dual-piston",
          "REAR BRAKE: Single 240 mm disc Caliper, Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.2 in OR 945 mm",
          "WHEELBASE: 58.1 in OR 1476 mm",
          "GROUND CLEARANCE: 13 in OR 330 mm",
          "WEIGHT: 233 lbs OR 106 kg",
          "FUEL CAPACITY: 1.6 gallons OR 6.1 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}