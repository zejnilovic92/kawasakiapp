import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataFourFive {
  static BikeObject bike1 = BikeObject(
    " KX250F 2004 - 2005",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/four_five/img1.png",
      "assets/images/kawasaki_two_fifty/four_five/img2.png",
      "assets/images/kawasaki_two_fifty/four_five/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2004 Kawasaki KX250F is a 4-stroke motocross motorcycle loaded with a no-nonsense 249cc single-cylinder engine capable of delivering a staggering 43 PS max power, and being suitable for both intermediate and experienced riders.",
          "This bike comes with lightweight rims and cast aluminium hubs for complying with the rigors of racing over rough terrain, while the 16-way adjustable forks provide excellent damping on any surface, no matter how hard you hit it. Disc brakes are used for the 2004 Kawasaki KX250F to make sure there is enough stopping force for the most demanding riding styles.",
          "Since the 2004 Kawasaki KX250F is not a street-legal bike, you're supposed to ride it on the tracks or non-public roads/ wilderness trails."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 12.6:1",
          "HORSEPOWER: 43/11000 KW(hp)/RPM",
          "TORQUE: 21/8500 lb-ft/RPM OR 28/8500 Nm/RPM",
          "FUEL SYSTEM: Carburettor: Keihin FCR37",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, high-tensile steel with D-section tubes for upper frame rails",
          "FRONT SUSPENSION: 48 mm upside-down cartridge-type telescopic fork with 16-way compression and 16-way rebound damping",
          "REAR SUSPENSION: New Uni-Trak with adjustable preload, 16-way compression and 16-way rebound damping",
          "FRONT BRAKE: Single semi-floating 250 mm disc, Dual-piston caliper",
          "REAR BRAKE: Single 240 mm disc, Single-piston caliper",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 33.1 in OR 841 mm",
          "SEAT HEIGHT: 37.8 in OR 960 mm",
          "WHEELBASE: 58.1 in OR 1476 mm",
          "GROUND CLEARANCE: -",
          "WEIGHT: 204 lbs OR 93 kg",
          "FUEL CAPACITY: 2 gallons OR 7.6 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}