import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataThirteenFourteen {
  static BikeObject bike1 = BikeObject(
    "KX250F 2013 - 2014",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/thirteen_fourteen/img1.png",
      "assets/images/kawasaki_two_fifty/thirteen_fourteen/img2.png",
      "assets/images/kawasaki_two_fifty/thirteen_fourteen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2014 MY Kawasaki KX250F comes with the same 249cc single engine as its predecessors, but the Japanese engineers have introduced some other new technologies to improve the KX250F's performance and carry one the heritage of this off-road performer.",
          "The launch control can be activated while in neutral, the first or second gear to maximize traction with the push of a button. It maximizes the take-off thrust, bettering the odds of a holeshot start.",
"The transmission has been redesigned, too. It now has four dogs instead of three, and this offers smoother shifts. Also, the shift fork grooves have been improved to enhance feel at the shift lever.",
          "Also, the grips have shorter barrels and are softer to improve rider comfort."

        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.8:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin) with dual injection",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: g: 48 mm inverted Separate Function front Fork (SFF) Type 2 with 22-position compression, 20-position rebound damping and 40-position spring preload",
          "REAR SUSPENSION: ni-Trac linkage system shock with 19-way low-speed and 4-turn high-speed compression damping, 22-way rebound damping and fully adjustable spring preload",
          "FRONT BRAKE: Single semi-floating 250 mm disc Caliper, Dual-piston",
          "REAR BRAKE: Single 240 mm disc Caliper, Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.2 in OR 945 mm",
          "WHEELBASE: 58.1 in OR 1476 mm",
          "GROUND CLEARANCE: 13 in OR 330 mm",
          "WEIGHT: 233 lbs OR 106 kg",
          "FUEL CAPACITY: 1.6 gallons OR 6.1 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}