import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataNineTen {
  static BikeObject bike1 = BikeObject(
    "KX250F 2009 - 2010",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/nine_ten/img1.png",
      "assets/images/kawasaki_two_fifty/nine_ten/img2.png",
      "assets/images/kawasaki_two_fifty/nine_ten/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2009 Kawasaki KX250F is a motocross motorcycle powered by a 249cc liquid-cooled single-cylinder engine.",
          "Thanks to its lightweight construction, this bike can also be used on other types of dirt tracks and even trails in the wilderness. Be advised that it is not street-legal.",
          "The new year model comes with a higher-grade radiator, less prone to mud build-up, with a thicker profile which makes reinforcement brackets unnecessary, thus shaving off some weight. The titanium-coated Showa dual-chamber forks and revised rear suspension now offer even better shock damping and eliminate the “kicking” effect.",
          "The synthetic skid plate offers the same sturdiness of the old aluminium one but with less mass. The 2009 KX250F sports the 2008 wide, serrated footpegs, factory-style Renthal handlebar and racing-specced petal rotors. The slim shrouds offer enhanced riding ergonomics."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.2:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: Carburettor: Keihin FCR-MX37",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 47mm upside-down cartridge-type telescopic fork with 16-way compression and 16-way rebound damping",
          "REAR SUSPENSION: New Uni-Trak with adjustable preload, dual-speed (low: 13-way, high: 2-turns or more) compression damping and 17-way rebound damping",
          "FRONT BRAKE: Single semi-floating 250 mm disc, Dual-piston",
          "REAR BRAKE: Single 240 mm disc, Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: 57.9 in OR 1471 mm",
          "GROUND CLEARANCE: 13.4 in OR 340 mm",
          "WEIGHT: 232 lbs OR 105 kg",
          "FUEL CAPACITY: 2.1 gallons OR 7.9 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}