import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataSevenEight {
  static BikeObject bike1 = BikeObject(
    "KX250F 2007 - 2008",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/seven_eight/img1.png",
      "assets/images/kawasaki_two_fifty/seven_eight/img2.png",
      "assets/images/kawasaki_two_fifty/seven_eight/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        ["Borrowing even more from the factory racers, the 2007 Kawasaki KX250F 249cc is taking further steps towards the modern motocross bikes.",
        "Lighter ribless hubs, back alumite-coated rims, short and lightweight silencers, a revised, hardened crankcase and a high-revving engine, they all add up to prove the superiority of the 2007 Kawasaki KX250F over the previous years models.",
        "The engine is loaded in repositioned mounts and the rear caliper guard ensures protection during racing on rough terrain, while the Showa twin-chamber fork delivers a stable damping performance. The piggyback rear shock sports enhanced adjustability to make the KX250F respond as needed to the racing abuse. Petal disc brakes are used to make sure there is enough stopping force for the most demanding riding styles, while their design keeps brakes clean and in good condition during longer races.",
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.5:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: Carburettor: Keihin FCR37",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 47mm upside-down cartridge-type telescopic fork with 16-way compression and 16-way rebound damping",
          "REAR SUSPENSION: New Uni-Trak with adjustable preload, dual-speed (low: 13-way, high: 2-turns or more) compression damping and 17-way rebound damping",
          "FRONT BRAKE: Single semi-floating 250 mm disc, Dual-piston",
          "REAR BRAKE: Single 240 mm disc, Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85 in OR 2159 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: 57.8 in OR 1468 mm",
          "GROUND CLEARANCE: 13.2 in OR 335 mm",
          "WEIGHT: 204 lbs OR 93 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}