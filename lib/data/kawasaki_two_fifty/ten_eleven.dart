import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataTenEleven {
  static BikeObject bike1 = BikeObject(
    "KX250F 2010 - 2011",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/ten_eleven/img1.png",
      "assets/images/kawasaki_two_fifty/ten_eleven/img2.png",
      "assets/images/kawasaki_two_fifty/ten_eleven/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "2010 brings fuel injection for the Kawasaki KX250F, transforming it into an even better motocross machine.",
        "The electronic injection provides lag-free acceleration, with a more predictable and smoother power deployment, dramatically changing the feel of the bike.",
          "A new Showa fork has been installed, with separate functions for each leg: damping for the left one and spring for the right one, providing far better response on rough surfaces, with smooth yet firm action that's hard to reproduce in conventional forks.",
          "The new throttle body also brings the opportunity for engine mapping tweaking, using the KX FI Calibration Kit. Custom mappings can be easily made while storing and analyzing the riding data can provide great racing improvements for the expert rider."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.5:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: Fuel Injection: 43mm x 1 (Keihin)",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 47 mm upside-down telescopic Separate Function front Fork (SFF) Compression damping: 22-way Rebound damping: 20-way Spring preload: 60-way",
          "REAR SUSPENSION: New Uni-Trak Compression damping:13-way (low-speed), 2-turns or more (high-speed) Rebound damping: 17-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single semi-floating 250 mm disc Caliper, Dual-piston",
          "REAR BRAKE: Single 240 mm disc Caliper, Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.2 in OR 945 mm",
          "WHEELBASE: 58.1 in OR 1476 mm",
          "GROUND CLEARANCE: 13 in OR 330 mm",
          "WEIGHT: 233 lbs OR 106 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}