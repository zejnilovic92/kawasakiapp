import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataSeventeenEighteen {
  static BikeObject bike1 = BikeObject(
    "KX250F 2017 - 2018",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/seventeen_eighteen/img1.png",
      "assets/images/kawasaki_two_fifty/seventeen_eighteen/img2.png",
      "assets/images/kawasaki_two_fifty/seventeen_eighteen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
         "The KX250F comes equipped with a 249cc engine, which is slightly smaller than average for a 4-stroke motor.",
          "Its seat measures 37.2 inches off the ground, a height that's only slightly higher than the norm and will accommodate riders of many sizes. The 1.61 gallon fuel tank is 15% smaller than average in this class, meaning the KX250F gives up some range to its competitors."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Single cylinder, 4-stroke, water-cooled",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.7:1",
          "HORSEPOWER: -/- KW(hp)/RPM",
          "TORQUE: 0/- lb-ft/RPM OR 0 Nm/RPM",
          "FUEL SYSTEM: DFI w/43mm Keihin throttle body and dual injectors",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48mm inverted Showa SFF telescopic fork with 40-way spring preload adjustability and 22-position compression and 20-position rebound damping adjustability/12.2 in",
          "REAR SUSPENSION: Uni-Trak linkage system and Showa shock with 19-position low-speed and 4-turns high-speed compression damping, 22-position rebound damping, fully adjustable spring preload/12.2 in",
          "FRONT BRAKE: Single semi-floating 270mm Braking petal disc with dual-piston caliper",
          "REAR BRAKE: Single 240mm Braking petal disc with single-piston caliper",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37 in OR 940 mm",
          "WHEELBASE: 58.1 in OR 1476 mm",
          "GROUND CLEARANCE: 12.6 in OR 320 mm",
          "WEIGHT: 229 lbs OR 104 kg",
          "FUEL CAPACITY: 1.7 gallons OR 6.4 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21",
          "REAR: 100/90-19",
        ],
      ),
    ],
  );
}