import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataFourteenFifteen {
  static BikeObject bike1 = BikeObject(
    "KX250F 2014 - 2015",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/fourteen_fifteen/img1.png",
      "assets/images/kawasaki_two_fifty/fourteen_fifteen/img2.png",
      "assets/images/kawasaki_two_fifty/fourteen_fifteen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2015 MY Kawasaki KX250F sports some new features and improvements to make the 250cc class two-wheeler perform even better on the dirt.",
          "New revised damping settings in the Showa Separate Function Fork (SFF) and revised Uni-Trak rear shock settings make the bike more responsive and easier to handle. The new adjustable footpegs and handlebar clamps mean that a wider variety of riders may enjoy it, no matter their inseam or arm length.",
          "Also, the brakes are new, with 270mm front and 240mm back rotors. This means more efficient braking before turns and improved overall control over the two-wheeler.",

        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.8:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin) with dual injection",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: g: 48 mm inverted Separate Function front Fork (SFF) Type 2 with 22-position compression, 20-position rebound damping and 40-position spring preload",
          "REAR SUSPENSION: Uni-Trak linkage system shock with 19-way low-speed and 4-turn high-speed compression damping, 22-way rebound damping and fully adjustable spring preload",
          "FRONT BRAKE: single semi-floating 270 mm petal disc",
          "REAR BRAKE: single 240 mm petal disc",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.2 in OR 945 mm",
          "WHEELBASE: 58.1 in OR 1476 mm",
          "GROUND CLEARANCE: 13 in OR 330 mm",
          "WEIGHT: 233 lbs OR 106 kg",
          "FUEL CAPACITY: 1.6 gallons OR 6.1 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}