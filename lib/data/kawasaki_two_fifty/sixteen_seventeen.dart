import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataSixteenSeventee {
  static BikeObject bike1 = BikeObject(
    "KX250F 2016 - 2017",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/sixteen_seventeen/img1.png",
      "assets/images/kawasaki_two_fifty/sixteen_seventeen/img2.png",
      "assets/images/kawasaki_two_fifty/sixteen_seventeen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "Speed, power, innovation and great looks, the 2016 KX250F is set to impress.",
          "With more urge, our new motocrosser is crafted to reduce lap times benefitting from engine and chassis weight reductions and a raft of engineering innovations. Building on a winning package, the new KX250F takes MX2 to the next level.",

        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, four-stroke, Single cylinder",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.7:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 with dual injection",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm inverted telescopic Separate Function front Fork (SFF) Type 2. Compression damping: 22-way. Rebound damping: 20-way. Spring preload:40-way",
          "REAR SUSPENSION: Uni-Trak, compression damping: 19-way (low-speed), 4 turns (high-speed). Rebound damping: 22-way. Spring preload: Fully adjustable.",
          "FRONT BRAKE: Single disc 270mm",
          "REAR BRAKE: Single disc 240mm",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37 in OR 940 mm",
          "WHEELBASE: 58.1 in OR 1476 mm",
          "GROUND CLEARANCE: 12.6 in OR 320 mm",
          "WEIGHT: 229 lbs OR 104 kg",
          "FUEL CAPACITY: 1.7 gallons OR 6.4 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21",
          "REAR: 100/90-19",
        ],
      ),
    ],
  );
}