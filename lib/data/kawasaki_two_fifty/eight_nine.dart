import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class TDataEightNine {
  static BikeObject bike1 = BikeObject(
    "KX250F 2008 - 2009",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_two_fifty/eight_nine/img1.png",
      "assets/images/kawasaki_two_fifty/eight_nine/img2.png",
      "assets/images/kawasaki_two_fifty/eight_nine/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        ["The 2008 version of the Kawasaki KX250F sports an even slimmer frame made of aluminium, with revised reinforcement to provide dependable rigidity.",
        "To withstand the rigors of motocross racing, the 2009 bike got Showa dual-chamber forks and a redesigned swingarm, while the previous model's rear shock retained the hardware specs, but received upgraded damping characteristics.",
        "A synthetic skid plate was installed, offering the same sturdiness the aluminium one provided but with a lighter design. Wider, serrated footpegs are now in place, to complement other nifty hardware upgrades, such as the factory-style Renthal handlebar and racing-specced petal rotors. If middleweight 4-stroke machines are your game, then the 2008 Kawasaki KX250F is the right toy."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 249 cm3",
          "BORE X STROKE: 3.0x2.1 in OR 76 mm",
          "COMPRESSION RATIO: 13.2:1",
          "HORSEPOWER: 43/11500 KW(hp)/RPM",
          "TORQUE: 22/9000 lb-ft/RPM OR 30/9000 Nm/RPM",
          "FUEL SYSTEM: Carburettor: Keihin FCR-MX37",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 47mm upside-down cartridge-type telescopic fork with 16-way compression and 16-way rebound damping",
          "REAR SUSPENSION: New Uni-Trak with adjustable preload, dual-speed (low: 13-way, high: 2-turns or more) compression damping and 17-way rebound damping",
          "FRONT BRAKE: Single semi-floating 250 mm disc, Dual-piston",
          "REAR BRAKE: Single 240 mm disc, Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.4 in OR 2169 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: 57.8 in OR 1468 mm",
          "GROUND CLEARANCE: 13.4 in OR 340 mm",
          "WEIGHT: 230 lbs OR 104 kg",
          "FUEL CAPACITY: 2.1 gallons OR 7.9 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 100/90-19 57M",
        ],
      ),
    ],
  );
}