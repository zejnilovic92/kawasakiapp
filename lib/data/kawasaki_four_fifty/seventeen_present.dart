import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataSeventeenPresent {
  static BikeObject bike1 = BikeObject(
    "KX450F 2017 - Present",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/seventeen_present/img1.png",
      "assets/images/kawasaki_four_fifty/seventeen_present/img2.png",
      "assets/images/kawasaki_four_fifty/seventeen_present/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2017 kx450F motorcycle is the most powerful, lightweight, and agile kx450F ever.",
          "Developed from the highest levels of racing, this championship bike has advanced technology sourced straight from the world’s premier race team—another reason why the kx450F is The Bike That Builds Champions. The characteristics of the 450cc engine are similar to those of the 2016 model. Last year's engine had a pop that was hard to escape, but Kawasaki seems to have remedied this for 2017."

        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.8:1",
          "HORSEPOWER: -/- KW(hp)/RPM",
          "TORQUE: 0/- lb-ft/RPM OR 0 Nm/RPM",
          "FUEL SYSTEM: DFI w/43mm Keihin throttle body",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed",
          "CLUTCH: Wet multi-disc",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: Inverted Showa SFF-Air TAC fork with Triple Air Chamber, DLC coated sliders, 22-position compression and 20-position rebound damping adjustment/12.0 in",
          "REAR SUSPENSION: Uni-Trak linkage system and Showa shock, 19-position low-speed and 4-turns high-speed compression damping, 22-position rebound damping and fully adjustable spring preload/12.0 in",
          "FRONT BRAKE: Single semi-floating 270mm Braking petal disc with dual-piston caliper",
          "REAR BRAKE: Single 240mm Braking petal disc with single-piston caliper",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 86.4 in OR 2195 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.4 in OR 950 mm",
          "WHEELBASE: 58.9 in OR 1496 mm",
          "GROUND CLEARANCE:  13.2 in OR 335 mm",
          "WEIGHT: 240 lbs OR 109 kg",
          "FUEL CAPACITY: 1.66 gallons OR 6.3 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21",
          "REAR: 120/80-19",
        ],
      ),
    ],
  );
}