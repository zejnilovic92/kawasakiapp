import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataElevenTwelveLimit {
  static BikeObject bike1 = BikeObject(
    "KX450F Leo Vince Limited Edition 2011 - 2012",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/eleven_twelve_limited/img1.png",
      "assets/images/kawasaki_four_fifty/eleven_twelve_limited/img2.png",
      "assets/images/kawasaki_four_fifty/eleven_twelve_limited/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2011 Kawasaki KX450F Leo Vince is a limited edition motocross bike, obviously equipped with an aftermarket, high-performance exhaust which will not only boost the brawn of the bike, but will also look great and sound even better.",
          "With a narrower frame sporting better maneuverability and the same race-worthy rigidity, the 2011 Kawasaki KX450F Leo Vince also comes with high-tech, fully-adjustable suspensions to provide the best feeling on any surface. Three standard engine mappings can be easily switched to by simply connecting the corresponding couplers, but the KX FI Calibration Kit lets the experienced rider fine-tune the bike to obtain the perfect response.",
          "Even more, riders can activate the Launch Control mode with just a press of a button and enjoy the factory-specced engine mapping that provides the best starts: maximal power and thrust with minimal wheel slip."
          ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.5:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin)",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork Compression damping: 22-way Rebound damping: 20-way",
          "REAR SUSPENSION: New Uni-Trak Compression damping: 22-way (low-speed), 2-turns or more (high-speed) Rebound damping: 22-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single rigid-mounted 250 mm petal disc Caliper: Dual-piston",
          "REAR BRAKE: Single 240 mm petal disc Caliper: Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.8 in OR 2179 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: 58.3 in OR 1481 mm",
          "GROUND CLEARANCE:  13 in OR 330 mm",
          "WEIGHT: 250 lbs OR 113 kg",
          "FUEL CAPACITY: 1.6 gallons OR 6.1 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 90/100-21 51M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}