import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataFourteenFifteen {
  static BikeObject bike1 = BikeObject(
    "KX450F 2014 - 2015",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/fourteen_fifteen/img1.png",
      "assets/images/kawasaki_four_fifty/fourteen_fifteen/img2.png",
      "assets/images/kawasaki_four_fifty/fourteen_fifteen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2015 MY Kawasaki KX450F sports some new features to help the rider overcome almost any obstacle or steep gradient the trail may throw at him.",
          "A new Showa SFF-Air TAC Fork with a triple air chamber means reduced overall weight of the bike, by replacing the classic metal spring with compressed air. This means a nimbler bike without affecting the precise feel of the suspension. A high-pressure 300 psi digital air pump also comes standard with every purchase.",
          "The Uni-Trak rear suspension has been fitted with a new Showa shock, which is firmer. This improves high-speed handling, provides greater stability and makes it more responsive when landing from jumps."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.5:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: EFI, 43 mm Keihin throttle body",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 49 mm inverted telescopic Separate Function front Fork Air - Triple Air Chamber (SFF-Air TAC)",
          "REAR SUSPENSION: Uni-Trak linkage system and Kayaba shock with 22-position low-speed and 2-turns or more high-speed compression damping, 22-position rebound damping and fully adjustable spring preload",
          "FRONT BRAKE: Single rigid mount 270 mm petal disc",
          "REAR BRAKE: Single 240 mm petal disc",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.8 in OR 2179 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: 58.3 in OR 1481 mm",
          "GROUND CLEARANCE:  13 in OR 330 mm",
          "WEIGHT: 228 lbs OR 112 kg",
          "FUEL CAPACITY: 1.6 gallons OR 6.1 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}