import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataTenEleven {
  static BikeObject bike1 = BikeObject(
    "KX450F 2010 - 2011",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/ten_eleven/img1.png",
      "assets/images/kawasaki_four_fifty/ten_eleven/img2.png",
      "assets/images/kawasaki_four_fifty/ten_eleven/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2010 Kawasaki KX450F is a motocross bike designed with flawless performance in mind.",
          "Not only it sports factory-inspired components, such as the top-drawer revised piston and race-oriented suspensions, but it also comes with new features to make riding better. The ECU now misses the traditional battery whose weight was stripped off the KX450F; the fuel injection system now needs no more adjusting on the fly depending on climate and track.",
          "Expert riders can map their own ignition settings thanks to the optional KX FI Calibration Kit, able not only to tweak the engine characteristics, but also to record and analyze the gathered data. The fuel pump is not lighter and repositioning it ensured a more reliable operation.",
          "This 449cc single-cylinder dirt beast will surely offer experienced riders all the brawn and high-class technology they need to prove their best."
          ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.5:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin)",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork Compression damping: 22-way Rebound damping: 20-way",
          "REAR SUSPENSION: New Uni-Trak Compression damping: 22-way (low-speed), 2-turns or more (high-speed) Rebound damping: 22-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single semi-floating 250 mm petal disc Caliper: Dual-piston",
          "REAR BRAKE: Single 240 mm petal disc Caliper: Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 86 in OR 2184 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.8 in OR 960 mm",
          "WHEELBASE: 58.3 in OR 1481 mm",
          "GROUND CLEARANCE:  13.2 in OR 335 mm",
          "WEIGHT: 250 lbs OR 113 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 90/100-21 57M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}