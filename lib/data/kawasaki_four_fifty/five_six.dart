import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataFiveSix {
  static BikeObject bike1 = BikeObject(
    "KX450F 2005 - 2006",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/five_six/img1.png",
      "assets/images/kawasaki_four_fifty/five_six/img2.png",
      "assets/images/kawasaki_four_fifty/five_six/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "The 2005 Kawasaki KX450F is a large dirt and motocross machine, built with serious performance in mind.",
          "Definitely not for the beginners, the 2005 KX450F features a powerful 449cc 4-stroke racing-specced engine loaded with titanium valves and a compression release system making it easy to start.",
          "The aluminium frame and narrow-profile bodywork and radiator shrouds add valuable points on the maneuverability. The Kayaba Air-Oil-Separate fork provides excellent damping and countless setup possibilities; together with the fully-adjustable rear shock, the 2005 Kawasaki KX450F can be easily tuned to respond perfectly to almost any rider and riding style, while delivering top-drawer performance on any surface type.",
          "Large petal rotors provide powerful stopping force and help the rider tame this beast."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.0:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: Keihin FCR40",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 4-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork with 22-way compression and 20-way rebound damping",
          "REAR SUSPENSION: New Uni-Trak with adjustable preload, dual-speed (low: 22-way, high: 2-turns or more) compression damping and 22-way rebound damping",
          "FRONT BRAKE: Single semi-floating 250 mm petal disc, dual-piston",
          "REAR BRAKE: Single 240 mm petal disc, single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 86 in OR 2184 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 38 in OR 965 mm",
          "WHEELBASE: 58.5 in OR 1486 mm",
          "GROUND CLEARANCE: -",
          "WEIGHT: 220 lbs OR 100 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 90/100-21 57M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}