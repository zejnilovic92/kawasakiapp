import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataSevenEight {
  static BikeObject bike1 = BikeObject(
    "KX450F 2007 - 2008",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/seven_eight/img1.png",
      "assets/images/kawasaki_four_fifty/seven_eight/img2.png",
      "assets/images/kawasaki_four_fifty/seven_eight/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
         "After adding a 5th gear to the previous year model, Kawasaki took the KX450F to the drawing board once more, for the new season.",
          "The 2007 Kawasaki KX450F sports a much smoother-shifting 5-speed gearbox while retaining all its racing character and legendary reliability, not offering a more linear power distribution for predictable behavior.",
          "A revised clutch cable boot offers easier adjustability for more convenient setups on the fly, while the Kayaba Air-Oil-Separate fork and rear shock deliver countless setup possibilities for almost any rider, any riding style and any surface.",
          "The weight of the 2007 KX450F still is under 100 kg (220 lbs) and a narrow, sturdy aluminium frame, this motorcycle is far more easy to handle than you'd think. The Diamond-Like Carbon coated forks and fully-adjustable suspension system provide excellent damping while the new airbox with mesh arrestor helps offer more torque at low rpm.",

        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.0:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: Keihin FCR40",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork with 22-way compression and 20-way rebound damping",
          "REAR SUSPENSION: New Uni-Trak with adjustable preload, dual-speed (low: 22-way, high: 2-turns or more) compression damping and 22-way rebound damping",
          "FRONT BRAKE: Single semi-floating 250 mm petal disc, dual-piston",
          "REAR BRAKE: Single 240 mm petal disc, single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 86 in OR 2184 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 38 in OR 965 mm",
          "WHEELBASE: 58.5 in OR 1486 mm",
          "GROUND CLEARANCE: 13.6 in OR 345 mm",
          "WEIGHT: 220 lbs OR 100 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 90/100-21 57M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}