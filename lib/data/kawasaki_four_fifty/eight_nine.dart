import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataEightNine {
  static BikeObject bike1 = BikeObject(
    "KX450F 2008 - 2009",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/eight_nine/img1.png",
      "assets/images/kawasaki_four_fifty/eight_nine/img2.png",
      "assets/images/kawasaki_four_fifty/eight_nine/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "After adding a 5th gear to the 2006 year model, it was time for electronic fuel injection to be installed in the new 2008 Kawasaki KX450F.",
          "The Keihin carb has been replaced by a top-drawer fuel injection system providing an even better power distribution across the rpm range with zero lag.",
          "Frame got narrower while the swingarm was redesigned and a new Uni-Trak rear shock got a larger piston (50mm) for better efficiency. A Renthal race-type handlebar was also provided, together with wider serrated footpegs and a synthetic, lighter skid plate. With a 340 ground clearance, the 2008 KX450F is simply the best Kawasaki has offered to the release date."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.0:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin)",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork Compression damping: 22-way Rebound damping: 20-way",
          "REAR SUSPENSION: New Uni-Trak Compression damping: 22-way (low-speed), 2-turns or more (high-speed) Rebound damping: 22-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single semi-floating 250 mm petal disc, dual-piston",
          "REAR BRAKE: Single 240 mm petal disc, single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 86 in OR 2184 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 38 in OR 965 mm",
          "WHEELBASE: 58.3 in OR 1481 mm",
          "GROUND CLEARANCE: 13.4 in OR 340 mm",
          "WEIGHT: 247 lbs OR 112 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 90/100-21 57M (EUR/USA/CAN/AUS), 80/100-21 51M (JPN)",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}