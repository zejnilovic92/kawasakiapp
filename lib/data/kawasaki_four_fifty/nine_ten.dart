import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataNineTen {
  static BikeObject bike1 = BikeObject(
    "KX450F 2009 - 2010",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/nine_ten/img1.png",
      "assets/images/kawasaki_four_fifty/nine_ten/img2.png",
      "assets/images/kawasaki_four_fifty/nine_ten/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "After 2008 brought the first Kawasaki KX450F with electronic fuel injection, it was time to take things even further, so the 2008 model comes with a tweakable ECU, allowing expert riders to finely-tune their engines in order to get the best performance.",
          "The bike's computer is already self-adjusting according to the riding style, but the optional KX FI Calibration Kit is the ultimate tool.",
          "New Kayaba forks sporting the Diamond-Like Coating are present on the 2009 Kawasaki KX450F, complemented by a new-ratio Uni-Trak rear shock, while the revised piston and radiator provide a new level of performance and dependability.",
          "Ergonomics have also been updated, keeping the bodywork slim and comfortable for aggressive riding styles, while still making the bike look great."
          ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.0:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin)",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork Compression damping: 22-way Rebound damping: 20-way",
          "REAR SUSPENSION: New Uni-Trak Compression damping: 22-way (low-speed), 2-turns or more (high-speed) Rebound damping: 22-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single semi-floating 250 mm petal disc, dual-piston",
          "REAR BRAKE: Single 240 mm petal disc, single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 86 in OR 2184 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 38 in OR 965 mm",
          "WHEELBASE: 58.3 in OR 1481 mm",
          "GROUND CLEARANCE: 13.4 in OR 340 mm",
          "WEIGHT: 250 lbs OR 113 kg",
          "FUEL CAPACITY: 1.9 gallons OR 7.2 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 90/100-21 57M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}