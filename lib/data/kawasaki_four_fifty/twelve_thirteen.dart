import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataTwelveThirteen {
  static BikeObject bike1 = BikeObject(
    "KX450F 2012 - 2013",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/twelve_thirteen/img1.png",
      "assets/images/kawasaki_four_fifty/twelve_thirteen/img2.png",
      "assets/images/kawasaki_four_fifty/twelve_thirteen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        [
          "For 2012, Kawasaki KX450F receives nifty enhancements. This motocross bike is now loaded with even more factory-inspired components, such as the revised piston and ECU, a new frame, now made from both cast and extruded aluminium elements, a top-drawer fuel injection and pump system, capable to withstand all sorts of racing abuse and a 4-way adjustable handlebar.",
"The bike sports three standard engine mappings which can be easily switched to by simply connecting the corresponding couplers. However, the KX FI Calibration Kit lets the experienced rider fine-tune the bike to obtain the perfect response. Even more, riders can activate the Launch Control mode with just a press of a button and enjoy the factory-specced engine mapping that provides the best starts: maximal power and thrust with minimal wheel slip."

        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.5:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: 43 mm x 1 (Keihin)",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork Compression damping: 22-way Rebound damping: 20-way",
          "REAR SUSPENSION: New Uni-Trak Compression damping: 22-way (low-speed), 2-turns or more (high-speed) Rebound damping: 22-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single rigid-mounted 250 mm petal disc Caliper: Dual-piston",
          "REAR BRAKE: Single 240 mm petal disc Caliper: Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.8 in OR 2179 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: -",
          "GROUND CLEARANCE:  13 in OR 330 mm",
          "WEIGHT: 228 lbs OR 112 kg",
          "FUEL CAPACITY: 1.6 gallons OR 6.1 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}