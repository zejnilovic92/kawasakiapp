import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FDataThirteenFourteen {
  static BikeObject bike1 = BikeObject(
    "KX450F 2013 - 2014",
    ["", "", "", ""],
    [
      "assets/images/kawasaki_four_fifty/thirteen_fourteen/img1.png",
      "assets/images/kawasaki_four_fifty/thirteen_fourteen/img2.png",
      "assets/images/kawasaki_four_fifty/thirteen_fourteen/img3.png",
    ],
    [
      ModelInfo(
        "General Information",
        ["The 2014 MY of the Kawasaki KX450F comes with many features to ensure the rider will not be disappointed, no matter how difficult the trail may be.",
          "The bike sports features such as launch control, which reduces wheelspin when accelerating hard on slippery trails or the Kayaba Pneumatic Spring Fork (PSF) using compressed air instead of a traditional metal-coil spring. These new-tech forks reduce the overall weight of the bike, making it nimbler without impacting the precise feel of the steering. This big-bore off-roader also comes with a free Kawasaki Genuine PSF Air Fork Pump for easy adjustments of the fork pressure.",
          "Another new feature of the KX450F is represented by the narrower grips. Softer and more comfortable, they are mounted on an adjustable 4-position handlebar, which allows the rider to pick a suitable riding position, no matter the size of one's inseam or arm length."
        ],
      ),

      ModelInfo(
        "ENGINE SPECS",
        [
          "ENGINE TYPE: Liquid-cooled, 4-stroke Single",
          "DISPLACEMENT: 449 cm3",
          "BORE X STROKE: 3.8x2.4 in OR 97 mm",
          "COMPRESSION RATIO: 12.5:1",
          "HORSEPOWER: 56/8500 KW(hp)/RPM",
          "TORQUE: 37/7000 lb-ft/RPM OR 50/7000 Nm/RPM",
          "FUEL SYSTEM: EFI, 43 mm Keihin throttle body",
        ],
      ),
      ModelInfo(
        "TRANSMISSION SPECS",
        [
          "GEARBOX: 5-speed, return",
          "CLUTCH: Wet multi-disc, manual",
          "PRIMARY DRIVE: -",
          "PRIMARY DRIVE: Chain",
        ],
      ),
      ModelInfo(
        "CHASIS SPECS",
        [
          "FRAME: Perimeter, aluminium",
          "FRONT SUSPENSION: 48 mm upside-down AOS-type telescopic fork Compression damping: 22-way Rebound damping: 20-way",
          "REAR SUSPENSION: New Uni-Trak Compression damping: 22-way (low-speed), 2-turns or more (high-speed) Rebound damping: 22-way Spring preload: Fully adjustable",
          "FRONT BRAKE: Single rigid-mounted 250 mm petal disc Caliper: Dual-piston",
          "REAR BRAKE: Single 240 mm petal disc Caliper: Single-piston",
        ],
      ),
      ModelInfo(
        "DIMENSION SPECS",
        [
          "OVERALL LENGTH: 85.8 in OR 2179 mm",
          "OVERALL WIDTH: 32.3 in OR 820 mm",
          "SEAT HEIGHT: 37.6 in OR 955 mm",
          "WHEELBASE: 58.3 in OR 1481 mm",
          "GROUND CLEARANCE:  13 in OR 330 mm",
          "WEIGHT: 228 lbs OR 112 kg",
          "FUEL CAPACITY: 1.6 gallons OR 6.1 L"
        ],
      ),
      ModelInfo(
        "TIRES SPECS",
        [
          "FRONT: 80/100-21 51M",
          "REAR: 120/80-19 63M",
        ],
      ),
    ],
  );
}