import 'model_info.dart';

class BikeObject {
  String modelYearAndType;
  List<String> modelType = [];
  List<String> modelPictures = [];
  List<ModelInfo> informations = [];

  BikeObject(this.modelYearAndType, this.modelType, this.modelPictures,
      this.informations);

  List<String> get getModelPictures {
    return modelPictures;
  }
}
