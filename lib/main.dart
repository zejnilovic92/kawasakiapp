import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:kawasakiapp/data/kawasaki_four_fifty/seventeen_present.dart';
import 'package:kawasakiapp/data/kawasaki_two_fifty/twelve_thirteen.dart';
import 'package:url_launcher/url_launcher.dart';import 'custom_expansion_tile.dart' as custom;
import 'data/data_sets.dart';
import 'data/kawasaki_four_fifty/eight_nine.dart';
import 'data/kawasaki_four_fifty/eleven_twelve_limited.dart';
import 'data/kawasaki_four_fifty/eleven_twelve_unlimited.dart';
import 'data/kawasaki_four_fifty/five_six.dart';
import 'data/kawasaki_four_fifty/fourteen_fifteen.dart';
import 'data/kawasaki_four_fifty/nine_ten.dart';
import 'data/kawasaki_four_fifty/seven_eight.dart';
import 'data/kawasaki_four_fifty/six_seven.dart';
import 'data/kawasaki_four_fifty/sixteen_present.dart';
import 'data/kawasaki_four_fifty/ten_eleven.dart';
import 'data/kawasaki_four_fifty/thirteen_fourteen.dart';
import 'data/kawasaki_four_fifty/twelve_thirteen.dart';
import 'data/kawasaki_two_fifty/eight_nine.dart';
import 'data/kawasaki_two_fifty/eleven_twelve.dart';
import 'data/kawasaki_two_fifty/five_six.dart';
import 'data/kawasaki_two_fifty/four_five.dart';
import 'data/kawasaki_two_fifty/fourteen_fifteen.dart';
import 'data/kawasaki_two_fifty/nine_ten.dart';
import 'data/kawasaki_two_fifty/seven_eight.dart';
import 'data/kawasaki_two_fifty/seventeen_eighteen.dart';
import 'data/kawasaki_two_fifty/six_seven.dart';
import 'data/kawasaki_two_fifty/sixteen_seventeen.dart';
import 'data/kawasaki_two_fifty/ten_eleven.dart';
import 'data/kawasaki_two_fifty/thirteen_fourteen.dart';
import 'designs/first_design.dart';

enum Section { home,

  two_eight_nine,two_eleven_twelve,two_five_six, two_four_five, two_fourteen_fifteen,two_nine_ten,two_seven_eight,
  two_seventeen_eighteen,two_six_seven,two_sixteen_seventeen,two_ten_eleven,two_thirteen_fourteen,two_twelve_thirteen,

four_eight_nine,eleven_twelve,four_eleven_twelve_limited,four_eleven_twelve_unlimited,four_five_six,
four_fourteen_fifteen,four_nine_ten,four_seven_eight,four_seventeen_present,four_six_seven,four_sixteen_present,
four_ten_eleven,four_thirteen_fourteen,four_twelve_thirteen
}


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kawasaki app',
      theme: ThemeData(
        primarySwatch: Colors.green,
       // visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Kawasaki app'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  BannerAd bannerAd;
  DataSets dataSets = DataSets();

  BannerAd buildBanner() {
    return BannerAd(
        adUnitId: BannerAd.testAdUnitId,
        size: AdSize.banner,
        listener: (MobileAdEvent event) {
          print(event);
        });
  }


  Section selectedSection;

  @override
  initState() {
    super.initState();
    selectedSection = Section.home;
    FirebaseAdMob.instance.initialize(appId: FirebaseAdMob.testAppId);
    bannerAd = buildBanner()
      ..load();
  }

  @override
  void dispose() {
    bannerAd?.dispose();
    super.dispose();
  }

  _selectSection(Section section) {
    setState(() {
      selectedSection = section;
    });
  }

  void customLaunch(command) async {
    if (await canLaunch(command)) {
      await launch(command);
    } else {
      print(' could not launch $command');
    }
  }


  @override
  Widget build(BuildContext context) {
    bannerAd
      ..load()
      ..show();
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.green,
                  image: DecorationImage(
                      image: AssetImage("assets/images/logo.jpeg"),
                      fit: BoxFit.cover)),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 25.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    height: 50,
                    color: Colors.green,
                    child: FlatButton(onPressed: () {
                      Navigator.pop(context);
                      _selectSection(Section.home);
                    },
                      child: Center(child: Text(
                        "Home", style: TextStyle(fontSize: 18.0, color: Colors
                          .white),)),),
                  ),)
            ),

            Padding(
                padding: const EdgeInsets.only(top: 25.0),
                child: custom.ExpansionTile(
                  headerBackgroundColor: Colors.green,
                  iconColor: Colors.white,
                  title: Container(
                    width: double.infinity,
                    height: 50,
                    color: Colors.green,
                    child: Center(child: Text("Yamaha KX 250 F",
                      style: TextStyle(
                          fontSize: 18.0, color: Colors.white),)),),
                  children: <Widget>[
                    for (String s in dataSets.getSectionNamesTwoFifty())
                      FlatButton(child: Text(
                        s, style: TextStyle(fontSize: 14.0),
                      ),
                        onPressed: () {
                          Navigator.pop(context);

                          if (s == "2008 - 2009") _selectSection(Section.two_eight_nine);
                          if (s == "2011 - 2012") _selectSection(Section.two_eleven_twelve);
                          if (s == "2005 - 2006") _selectSection(Section.two_five_six);
                          if (s == "2004 - 2005") _selectSection(Section.two_four_five);
                          if (s == "2014 - 2015") _selectSection(Section.two_fourteen_fifteen);
                          if (s == "2009 - 2010") _selectSection(Section.two_nine_ten);
                          if (s == "2007 - 2008") _selectSection(Section.two_seven_eight);
                          if (s ==  "2017 - 2018") _selectSection(Section.two_seventeen_eighteen);
                          if (s ==  "2006 - 2007") _selectSection(Section.two_six_seven);
                          if (s ==  "2016 - 2017") _selectSection(Section.two_sixteen_seventeen);
                          if (s ==  "2010 - 2011") _selectSection(Section.two_ten_eleven);
                          if (s ==  "2013 - 2014") _selectSection(Section.two_thirteen_fourteen);
                          if (s ==  "2012 - 2013") _selectSection(Section.two_twelve_thirteen);

                        },
                      ),
                  ],
                )
            ),
            Padding(
                padding: const EdgeInsets.only(top: 25.0),
                child: custom.ExpansionTile(
                  headerBackgroundColor: Colors.green,
                  iconColor: Colors.white,
                  title: Container(
                    width: double.infinity,
                    height: 50,
                    color: Colors.green,
                    child: Center(child: Text("Kawasaki KX 450 F",
                      style: TextStyle(
                          fontSize: 18.0, color: Colors.white),)),),
                  children: <Widget>[
                    for (String s in dataSets.getSectionNamesFourFifty())
                      FlatButton(child: Text(
                        s, style: TextStyle(fontSize: 14.0),
                      ),
                        onPressed: () {
                          Navigator.pop(context);
                          if (s == "2008 - 2009") _selectSection(Section.four_eight_nine);
                          if (s == "2011 - 2012") _selectSection(Section.four_eleven_twelve_unlimited);
                          if (s == "Leo Vince Limited Edition 2011-2012") _selectSection(Section.four_eleven_twelve_limited);
                          if (s == "2005 - 2006") _selectSection(Section.four_five_six);
                          if (s == "2014 - 2015") _selectSection(Section.four_fourteen_fifteen);
                          if (s == "2009 - 2010") _selectSection(Section.four_nine_ten);
                          if (s == "2007 - 2008") _selectSection(Section.four_seven_eight);
                          if (s ==  "2017 - Present") _selectSection(Section.four_seventeen_present);
                          if (s ==  "2006 - 2007") _selectSection(Section.four_six_seven);
                          if (s ==  "2016 - Present") _selectSection(Section.four_sixteen_present);
                          if (s ==  "2010 - 2011") _selectSection(Section.four_ten_eleven);
                          if (s ==  "2013 - 2014") _selectSection(Section.four_thirteen_fourteen);
                          if (s ==  "2012 - 2013") _selectSection(Section.four_twelve_thirteen);
                        },
                      ),
                  ],
                )
            ),
            SizedBox(height: 70.0,)
          ],
        ), // Populate the Drawer in the next step.
      ),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _setScreen(),

    );
  }


  _homeScreen() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/kawasaki_two_fifty/four_five/img1.png'),
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              "This app is for Kawasaki KX enthusiasts who love everything KX!",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.0),
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              "I am going to be constantly updating this adding more and more content.",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.0),
            ),
            SizedBox(
              height: 15.0,
            ),
            RichText(
              textAlign: TextAlign.center,
              text: new TextSpan(
                children: [
                  new TextSpan(
                    text:
                    'If you want your content added here, shoot me an email at ',
                    style: new TextStyle(color: Colors.black, fontSize: 16.0),
                  ),
                  new TextSpan(
                    text: 'spencer@spencert.com',
                    style: new TextStyle(color: Colors.blue, fontSize: 16.0),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () {
                        customLaunch(
                            'mailto:spencer@spencert.com?subject=YamahaWR%20subject');
                      },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  _setScreen() {
    if (selectedSection == Section.home)
      return _homeScreen();

    else if (selectedSection == Section.two_eight_nine)
      return (FirstDesign.createScreen(TDataEightNine.bike1));
    else if (selectedSection == Section.two_eleven_twelve)
      return (FirstDesign.createScreen(TDataElevenTwelve.bike1));
    else if (selectedSection == Section.two_five_six)
      return (FirstDesign.createScreen(TDataFiveSix.bike1));
    else if (selectedSection == Section.two_four_five)
      return (FirstDesign.createScreen(TDataFourFive.bike1));
    else if (selectedSection == Section.two_fourteen_fifteen)
      return (FirstDesign.createScreen(TDataFourteenFifteen.bike1));
    else if (selectedSection == Section.two_nine_ten)
      return (FirstDesign.createScreen(TDataNineTen.bike1));
    else if (selectedSection == Section.two_seven_eight)
      return (FirstDesign.createScreen(TDataSevenEight.bike1));
    else if (selectedSection == Section.two_seventeen_eighteen)
      return (FirstDesign.createScreen(TDataSeventeenEighteen.bike1));
    else if (selectedSection == Section.two_six_seven)
      return (FirstDesign.createScreen(TDataSixSeven.bike1));
    else if (selectedSection == Section.two_sixteen_seventeen)
      return (FirstDesign.createScreen(TDataSixteenSeventee.bike1));
    else if (selectedSection == Section.two_ten_eleven)
      return (FirstDesign.createScreen(TDataTenEleven.bike1));
    else if (selectedSection == Section.two_thirteen_fourteen)
      return (FirstDesign.createScreen(TDataThirteenFourteen.bike1));
    else if (selectedSection == Section.two_twelve_thirteen)
      return (FirstDesign.createScreen(TDataTwelveThirteen.bike1));


    else if (selectedSection == Section.four_eight_nine)
      return (FirstDesign.createScreen(FDataEightNine.bike1));
    else if (selectedSection == Section.four_eleven_twelve_limited)
      return (FirstDesign.createScreen(FDataElevenTwelveLimit.bike1));
    else if (selectedSection == Section.four_eleven_twelve_unlimited)
      return (FirstDesign.createScreen(FDataElevenTwelveNoLimit.bike1));
    else if (selectedSection == Section.four_five_six)
      return (FirstDesign.createScreen(FDataFiveSix.bike1));
    else if (selectedSection == Section.four_fourteen_fifteen)
      return (FirstDesign.createScreen(FDataFourteenFifteen.bike1));
    else if (selectedSection == Section.four_nine_ten)
      return (FirstDesign.createScreen(FDataNineTen.bike1));
    else if (selectedSection == Section.four_seven_eight)
      return (FirstDesign.createScreen(FDataSevenEight.bike1));
    else if (selectedSection == Section.four_seventeen_present)
      return (FirstDesign.createScreen(FDataSeventeenPresent.bike1));
    else if (selectedSection == Section.four_six_seven)
      return (FirstDesign.createScreen(FDataSixSeven.bike1));
    else if (selectedSection == Section.four_sixteen_present)
      return (FirstDesign.createScreen(FDataSixteenPresent.bike1));
    else if (selectedSection == Section.four_ten_eleven)
      return (FirstDesign.createScreen(FDataTenEleven.bike1));
    else if (selectedSection == Section.four_thirteen_fourteen)
      return (FirstDesign.createScreen(FDataThirteenFourteen.bike1));
    else if (selectedSection == Section.four_twelve_thirteen)
      return (FirstDesign.createScreen(FDataTwelveThirteen.bike1));
  }





}

