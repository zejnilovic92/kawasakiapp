import 'package:flutter/material.dart';
import 'package:kawasakiapp/model/bike_object.dart';
import 'package:kawasakiapp/model/model_info.dart';

class FirstDesign {
  static createScreen(BikeObject bikeObject) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          bikeObject.modelYearAndType,
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Image(
                      image: AssetImage(bikeObject.modelPictures[0]),
                    ),
                    for (ModelInfo info in bikeObject.informations)
                      _getListTitle(info),
                    Padding(
                      padding: const EdgeInsets.only(right: 58.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          for (String s in bikeObject.modelPictures)
                            Image(
                              image: AssetImage(s),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 50.0,)

        ],
      ),
    );
  }

  static _getListTitle(ModelInfo info) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Colors.grey,
                ),
                color: Color(0XFFF2F2F2)),
            child: SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: Text(
                  info.infoName,
                  style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.red,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                for (String s in info.infoContent)
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("-" + s + "\n"),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
